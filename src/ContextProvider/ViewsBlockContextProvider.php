<?php

namespace Drupal\views_block_context_provider\ContextProvider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\Plugin\Block\ViewsBlock;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Exposes taxonomy terms to views blocks as context values in Layout Builder.
 *
 * This is a rather unusual context provider. Our goal is to expose as contexts
 * taxonomy terms to views blocks that have contextual filters. However, we
 * don't want these terms available in all places contexts are available (i.e.
 * blocks, panels, etc). Unfortunately we "have" to implement this interface
 * to make sure the contexts work properly when we do want them to be exposed.
 * Because of that, we only implement ::getAvailableContexts() and we try to
 * restrict its result as much as possible.
 *
 * When it's time to get the real contexts available, we alter the block
 * configuration form in a form_alter(), and artificially inject there the
 * extra context values we are interested in.
 *
 * @see views_block_context_provider_form_layout_builder_configure_block_alter()
 */
class ViewsBlockContextProvider implements ContextProviderInterface {

  use StringTranslationTrait;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * The current route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * Constructs a new ViewsBlockContextProvider object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match object.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $context_handler
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, RequestStack $request_stack, RouteMatchInterface $route_match, ContextHandlerInterface $context_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->config = $config_factory->get('views_block_context_provider.settings');
    $this->request = $request_stack->getCurrentRequest();
    $this->routeMatch = $route_match;
    $this->contextHandler = $context_handler;
  }

  /**
   * Helper to inject context mapping into a block configuration form.
   *
   * This expects a views block plugin + configuration form array. It's assumed
   * that, for each contextual filter that's configured in the view to validate
   * against taxonomy term IDs, the view accepts context values in the block
   * configuration form. That's the scenario we will try to affect here.
   *
   * There is also a possibility to further restrict the action of this method.
   * Site admins can do so by going to the global settings form, and unchecking
   * one or more vocabularies there. If a vocabulary is explicitly excluded
   * there, its terms will not be exposed as contexts here, even if the view
   * has a term contextual filter.
   *
   * @param \Drupal\views\Plugin\Block\ViewsBlock $plugin
   *   The views block plugin we are configuring.
   * @param array $form
   *   The form array, passed in by reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function injectRuntimeContextsForViewBlock(ViewsBlock $plugin, array &$form, FormStateInterface $form_state) {
    $view = $plugin->getViewExecutable();
    if (!$view) {
      return;
    }

    // Site admins can restrict which vocabularies should have their terms
    // exposed as contexts. Only include here the intersection between the
    // vocabs allowed in the global config and the ones the view has validation
    // for.
    $exposed_vocabs = $this->config->get('exposed_vocabularies') ?: [];
    if (empty($exposed_vocabs)) {
      // If none are selected, all are allowed.
      $vocabs = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
      if ($vocabs) {
        $exposed_vocabs = array_keys($vocabs);
      }
    }

    /** @var \Drupal\views\Plugin\views\display\Block $display */
    $display = $view->getDisplay();
    $contextual_filters = $display->getOption('arguments');
    if (empty($contextual_filters)) {
      return;
    }
    $new_context_elements = [];
    foreach ($contextual_filters as $field_name => $argument) {
      if (!empty($argument['validate']['type']) && $argument['validate']['type'] === 'entity:taxonomy_term') {
        // This contextual filter is validating against a vocabulary. Add it
        // as a context and inject its corresponding form element.
        if (!empty($argument['validate_options']['bundles'])) {
          $field_vocabs = array_intersect($exposed_vocabs, $argument['validate_options']['bundles']);
          if (empty($field_vocabs)) {
            continue;
          }
          $tids = $this->termStorage->getQuery()
            ->condition('vid', $field_vocabs, 'IN')
            ->execute();
          if (empty($tids)) {
            continue;
          }
          $contexts = $this->getContextsForTids($tids);
          if (!empty($contexts)) {
            // We want to use the fully-qualified context ID in the form options.
            foreach ($contexts as $unqualified_id => $context) {
              $qualified_id = "@views_block_context_provider.term_context:{$unqualified_id}";
              $contexts[$qualified_id] = $context;
              unset($contexts[$unqualified_id]);
            }
            $new_context_elements = array_merge($new_context_elements, $this->addContextAssignmentElement($plugin, $contexts, $field_name));
          }
        }
      }
    }

    if (!empty($new_context_elements)) {
      $form['settings']['context_mapping'] = $new_context_elements;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableContexts() {
    $contexts = [];

    // Bail out if this is not being requested from a place where our contexts
    // are expected (i.e. in a views block being configured in LB).
    if (!$this->appliesToCurrentRequest()) {
      return $contexts;
    }

    $exposed_vocabs = $this->config->get('exposed_vocabularies') ?: [];
    if (empty($exposed_vocabs)) {
      // If none are selected, all are allowed.
      $vocabs = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
      if ($vocabs) {
        $exposed_vocabs = array_keys($vocabs);
      }
    }

    $tids = $this->termStorage->getQuery()
      ->condition('vid', $exposed_vocabs, 'IN')
      ->execute();

    if (!empty($tids)) {
      $contexts = $this->getContextsForTids($tids);
    }

    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getRuntimeContexts(array $unqualified_context_ids) {
    return [];
  }

  /**
   * Helper to load and generate context objects for taxonomy term entities.
   *
   * @param array $tids
   *   An indexed list of TIDs we want to generate contexts for.
   *
   * @return \Drupal\Core\Plugin\Context\Context[]
   *   An associative array where keys are term UUIDs, and values are context
   *   objects for each of them.
   */
  private function getContextsForTids(array $tids) {
    $contexts = [];
    foreach ($this->termStorage->loadMultiple($tids) as $term) {
      $context = new Context(new ContextDefinition('entity:taxonomy_term', $term->label()), $term);
      $context->addCacheableDependency($term);
      $contexts[$term->uuid()] = $context;
    }
    return $contexts;
  }

  /**
   * Whether the contexts from this class should be exposed on this request.
   *
   * @return bool
   *   TRUE if:
   *   - This request refers to a views block configuration form inside LB;
   *   - This request is being served in the FE theme (non-admin route).
   *   FALSE otherwise.
   */
  private function appliesToCurrentRequest() {
    // When we are inside LB, configuring a views block, we obviously want to
    // expose our contexts.
    $is_lb_config_block = $this->request->isXmlHttpRequest() && strpos($this->routeMatch->getRouteName(), 'layout_builder.') === 0;
    // @todo Make this smarter... there could be another plugin inside LB
    // that uses contexts and where we also don't want to expose our stuff?
    if ($is_lb_config_block) {
      return TRUE;
    }

    // We will dangerously assume that if this is a request being served in the
    // FE theme, this is likely OK too, based on the assumption that it's
    // unlikely to have context-selection tools on the UI being displayed in the
    // front-end theme.
    $route = $this->routeMatch->getRouteObject();
    if ($route && !$route->getOption('_admin_route')) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Builds a form element for assigning a context to a given slot.
   *
   * NOTE: This is a copy of
   * \Drupal\Core\Plugin\ContextAwarePluginAssignmentTrait::addContextAssignmentElement()
   * with the exception that we only assign context to the form element that
   * matches the $arg_name we are passing in additionally here.
   *
   * @param \Drupal\Core\Plugin\ContextAwarePluginInterface $plugin
   *   The context-aware plugin.
   * @param \Drupal\Component\Plugin\Context\ContextInterface[] $contexts
   *   An array of contexts.
   * @param string $arg_name
   *   The name of the views contextual filter (argument).
   *
   * @return array
   *   A form element for assigning context.
   */
  private function addContextAssignmentElement(ContextAwarePluginInterface $plugin, array $contexts, $arg_name) {
    $element = [];
    foreach ($plugin->getContextDefinitions() as $context_slot => $definition) {
      if ($context_slot !== $arg_name) {
        continue;
      }

      $valid_contexts = $this->contextHandler->getMatchingContexts($contexts, $definition);
      $options = [];
      foreach ($valid_contexts as $context_id => $context) {
        $element['#tree'] = TRUE;
        $options[$context_id] = $context->getContextDefinition()->getLabel();
        $element[$context_slot] = [
          '#type' => 'value',
          '#value' => $context_id,
        ];
      }

      // Show the context selector only if there is more than 1 option to choose
      // from. Also, show if there is a single option but the plugin does not
      // require a context.
      if (count($options) > 1 || (count($options) == 1 && !$definition->isRequired())) {
        $assignments = $plugin->getContextMapping();
        $element[$context_slot] = [
          '#title' => $definition->getLabel() ?: $this->t('Select a @context value:', ['@context' => $context_slot]),
          '#type' => 'select',
          '#options' => $options,
          '#required' => $definition->isRequired(),
          '#default_value' => !empty($assignments[$context_slot]) ? $assignments[$context_slot] : '',
          '#description' => $definition->getDescription(),
        ];
        if (!$definition->isRequired()) {
          $element[$context_slot]['#empty_value'] = '';
        }
      }
    }
    return $element;
  }

}
