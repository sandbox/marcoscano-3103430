<?php

namespace Drupal\Tests\views_block_context_provider\FunctionalJavascript;

use Drupal\Core\Language\LanguageInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\contextual\FunctionalJavascript\ContextualLinkClickTrait;

/**
 * Tests the contexts provided by this module.
 *
 * @group views_block_context_provider
 */
class ViewsBlockContextProviderTest extends WebDriverTestBase {

  use ContextualLinkClickTrait;

  public static $modules = [
    'layout_builder',
    'layout_discovery',
    'block',
    'user',
    'taxonomy',
    'path',
    'text',
    'node',
    'contextual',
    'views',
    'views_block_context_provider',
    // The test module below packages some configuration we will assume existing
    // in these tests:
    // - 3 taxonomy vocabularies (Cats, Dogs, Goats)
    // - 1 "page" content type with a per-node LB override configured
    // - 3 taxonomy fields on this content type
    // - 1 view ("vbcp_node_test") with a block display, with 3 contextual
    //   filters configured, one per each field mentioned above.
    'vbcp_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->drupalLogin($this->drupalCreateUser([
      'configure any layout',
      'bypass node access',
      'administer node display',
      'administer node fields',
      'access contextual links',
      'administer site configuration',
      'administer blocks',
    ]));
  }

  public function testViewsBlockContextsInLayoutBuilder() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Create some terms in all 3 vocabularies:
    $term_boxer = Term::create([
      'name' => 'Boxer',
      'vid' => 'dogs',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $term_boxer->save();
    $term_labrador = Term::create([
      'name' => 'Labrador',
      'vid' => 'dogs',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $term_labrador->save();
    $term_siamese = Term::create([
      'name' => 'Siamese',
      'vid' => 'cats',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $term_siamese->save();
    $term_persian = Term::create([
      'name' => 'Persian',
      'vid' => 'cats',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $term_persian->save();
    $term_alpine = Term::create([
      'name' => 'Alpine',
      'vid' => 'goats',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $term_alpine->save();

    // And some nodes tagged with them.
    $node_untagged = Node::create([
      'title' => 'This node should never show up',
      'type' => 'page',
    ]);
    $node_untagged->save();
    $node_boxer = Node::create([
      'title' => 'Im a Boxer',
      'type' => 'page',
      'field_dog_type' => [$term_boxer->id()],
    ]);
    $node_boxer->save();
    $node_labrador = Node::create([
      'title' => 'Im a Labrador',
      'type' => 'page',
      'status' => 1,
      'field_dog_type' => [$term_labrador->id()],
    ]);
    $node_labrador->save();
    $node_siamese = Node::create([
      'title' => 'Im a Siamese',
      'type' => 'page',
      'field_cat_type' => [$term_siamese->id()],
    ]);
    $node_siamese->save();
    $node_persian = Node::create([
      'title' => 'Im a Persian',
      'type' => 'page',
      'field_cat_type' => [$term_persian->id()],
    ]);
    $node_persian->save();
    $node_goat = Node::create([
      'title' => 'Im a Goat',
      'type' => 'page',
      'field_goat_type' => [$term_alpine->id()],
    ]);
    $node_goat->save();

    // Finally, the node we will be adding stuff to the LB.
    /** @var \Drupal\node\NodeInterface $page_node */
    $page_node = Node::create([
      'title' => 'Im a LB-based page node.',
      'type' => 'page',
    ]);
    $page_node->save();

    $this->drupalGet("/node/{$page_node->id()}/layout");
    $page->clickLink('Add block');
    $this->assertNotEmpty($assert_session->waitForElementVisible('css', '#drupal-off-canvas'));
    $assert_session->assertWaitOnAjaxRequest();
    $this->saveHtmlOutput();
    // There is a link in the side tray to add our views block.
    $views_link = $assert_session->elementExists('css', '#drupal-off-canvas a:contains("vbcp_node_test")');
    $views_link->click();
    $this->assertNotEmpty($assert_session->waitForElementVisible('css', '#drupal-off-canvas input[value="Add block"]'));
    $this->saveHtmlOutput();
    $assert_session->elementExists('css', '#drupal-off-canvas select[name="settings[override][items_per_page]"]');
    // Initially all 3 filters configured in the view appear as contexts.
    $cat_select = $assert_session->elementExists('css', '#drupal-off-canvas select[name="settings[context_mapping][field_cat_type_target_id]"]');
    $dog_select = $assert_session->elementExists('css', '#drupal-off-canvas select[name="settings[context_mapping][field_dog_type_target_id]"]');
    $goat_select = $assert_session->elementExists('css', '#drupal-off-canvas select[name="settings[context_mapping][field_goat_type_target_id]"]');
    // Make sure their options is exactly what we expect, nothing else.
    // Cats.
    $this->assertSame(3, count($cat_select->findAll('css', 'option')));
    // Initially the "- None -" option should be selected.
    $selected_cat_option = $assert_session->elementExists('css', 'option[selected="selected"]', $cat_select);
    $this->assertSame('- None -', $selected_cat_option->getText());
    // Other terms should be there too.
    $siamese_option = $assert_session->elementExists('css', "option[value='@views_block_context_provider.term_context:{$term_siamese->uuid()}']", $cat_select);
    $this->assertSame('Siamese', $siamese_option->getText());
    $persian_option = $assert_session->elementExists('css', "option[value='@views_block_context_provider.term_context:{$term_persian->uuid()}']", $cat_select);
    $this->assertSame('Persian', $persian_option->getText());
    // Dogs.
    $this->assertSame(3, count($dog_select->findAll('css', 'option')));
    $selected_dog_option = $assert_session->elementExists('css', 'option[selected="selected"]', $dog_select);
    $this->assertSame('- None -', $selected_dog_option->getText());
    $boxer_option = $assert_session->elementExists('css', "option[value='@views_block_context_provider.term_context:{$term_boxer->uuid()}']", $dog_select);
    $this->assertSame('Boxer', $boxer_option->getText());
    $labrador_option = $assert_session->elementExists('css', "option[value='@views_block_context_provider.term_context:{$term_labrador->uuid()}']", $dog_select);
    $this->assertSame('Labrador', $labrador_option->getText());
    // Goats.
    $this->assertSame(2, count($goat_select->findAll('css', 'option')));
    $selected_goat_option = $assert_session->elementExists('css', 'option[selected="selected"]', $goat_select);
    $this->assertSame('- None -', $selected_goat_option->getText());
    $alpine_option = $assert_session->elementExists('css', "option[value='@views_block_context_provider.term_context:{$term_alpine->uuid()}']", $goat_select);
    $this->assertSame('Alpine', $alpine_option->getText());

    // Verify that selects appear with their Views filter labels, and if users
    // configure an "Administrative label" in the contextual filter options,
    // it shows up accordingly (in this case this view is configured to have a
    // customized label for the goat filter).
    $this->assertSame('Content: Cat type (field_cat_type)', $assert_session->elementExists('css', '#drupal-off-canvas .form-item-settings-context-mapping-field-cat-type-target-id label')->getText());
    $this->assertSame('Content: Dog type (field_dog_type)', $assert_session->elementExists('css', '#drupal-off-canvas .form-item-settings-context-mapping-field-dog-type-target-id label')->getText());
    $this->assertSame('Goat type', $assert_session->elementExists('css', '#drupal-off-canvas .form-item-settings-context-mapping-field-goat-type-target-id label')->getText());

    // Select values from filters and check views results appear accordingly.
    $page->selectFieldOption('Content: Cat type (field_cat_type)', "@views_block_context_provider.term_context:{$term_siamese->uuid()}");
    $page->pressButton('Add block');
    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas', 5000);
    $assert_session->pageTextContains('You have unsaved changes');
    $this->saveHtmlOutput();
    // The preview already displays what we want.
    $assert_session->pageTextNotContains('This node should never show up');
    $block_selector = '.field--name-layout-builder__layout .block-views-blockvbcp-node-test-block-1';
    $block_preview = $assert_session->elementExists('css', $block_selector);
    $views_rows = $block_preview->findAll('css', '.views-row');
    $this->assertSame(1, count($views_rows));
    $assert_session->elementTextContains('css', $block_selector, 'vbcp_node_test');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Siamese');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Boxer');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Goat');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Persian');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Labrador');

    // In the front-end we should see the same.
    $page->pressButton('Save layout');
    $assert_session->pageTextContains('The layout override has been saved');
    $block_selector = '.layout__region--content .block-views-blockvbcp-node-test-block-1';
    $block_rendered = $assert_session->elementExists('css', $block_selector);
    $views_rows = $block_rendered->findAll('css', '.views-row');
    $this->assertSame(1, count($views_rows));
    $assert_session->elementTextContains('css', $block_selector, 'vbcp_node_test');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Siamese');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Boxer');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Goat');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Persian');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Labrador');

    // Change some contexts, verify the views changes accordingly.
    $block_selector = '.field--name-layout-builder__layout .block-views-blockvbcp-node-test-block-1';
    $this->visitLayoutAndOpenBlockConfigForm($page_node, $block_selector);
    $page->selectFieldOption('Content: Cat type (field_cat_type)', '- None -');
    $page->selectFieldOption('Content: Dog type (field_dog_type)', "@views_block_context_provider.term_context:{$term_boxer->uuid()}");
    $page->pressButton('Update');
    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas', 5000);
    $assert_session->pageTextContains('You have unsaved changes');
    $this->saveHtmlOutput();

    // Preview now has only Boxer.
    $assert_session->pageTextNotContains('This node should never show up');
    $block_selector = '.field--name-layout-builder__layout .block-views-blockvbcp-node-test-block-1';
    $block_preview = $assert_session->elementExists('css', $block_selector);
    $views_rows = $block_preview->findAll('css', '.views-row');
    $this->assertSame(1, count($views_rows));
    $assert_session->elementTextContains('css', $block_selector, 'vbcp_node_test');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Siamese');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Boxer');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Goat');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Persian');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Labrador');

    // Front-end too.
    $page->pressButton('Save layout');
    $assert_session->pageTextContains('The layout override has been saved');
    $block_selector = '.layout__region--content .block-views-blockvbcp-node-test-block-1';
    $block_rendered = $assert_session->elementExists('css', $block_selector);
    $views_rows = $block_rendered->findAll('css', '.views-row');
    $this->assertSame(1, count($views_rows));
    $assert_session->elementTextContains('css', $block_selector, 'vbcp_node_test');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Siamese');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Boxer');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Goat');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Persian');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Labrador');

    // Combining multiple contexts works too.
    $multi_animal_node = Node::create([
      'title' => 'Im a Multi-animal!',
      'type' => 'page',
      'field_cat_type' => [$term_siamese->id()],
      'field_dog_type' => [$term_boxer->id()],
      'field_goat_type' => [$term_alpine->id()],
    ]);
    $multi_animal_node->save();
    $block_selector = '.field--name-layout-builder__layout .block-views-blockvbcp-node-test-block-1';
    $this->visitLayoutAndOpenBlockConfigForm($page_node, $block_selector);
    $page->selectFieldOption('Content: Cat type (field_cat_type)', "@views_block_context_provider.term_context:{$term_siamese->uuid()}");
    $page->selectFieldOption('Content: Dog type (field_dog_type)', "@views_block_context_provider.term_context:{$term_boxer->uuid()}");
    $page->selectFieldOption('Goat type', "@views_block_context_provider.term_context:{$term_alpine->uuid()}");
    $page->pressButton('Update');
    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas', 5000);
    $assert_session->pageTextContains('You have unsaved changes');
    $this->saveHtmlOutput();

    // Preview now has only Multi-animal.
    $assert_session->pageTextNotContains('This node should never show up');
    $block_selector = '.field--name-layout-builder__layout .block-views-blockvbcp-node-test-block-1';
    $block_preview = $assert_session->elementExists('css', $block_selector);
    $views_rows = $block_preview->findAll('css', '.views-row');
    $this->assertSame(1, count($views_rows));
    $assert_session->elementTextContains('css', $block_selector, 'vbcp_node_test');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Siamese');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Boxer');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Goat');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Persian');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Labrador');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Multi-animal!');

    // Front-end too.
    $page->pressButton('Save layout');
    $assert_session->pageTextContains('The layout override has been saved');
    $block_selector = '.layout__region--content .block-views-blockvbcp-node-test-block-1';
    $block_rendered = $assert_session->elementExists('css', $block_selector);
    $views_rows = $block_rendered->findAll('css', '.views-row');
    $this->assertSame(1, count($views_rows));
    $assert_session->elementTextContains('css', $block_selector, 'vbcp_node_test');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Siamese');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Boxer');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Goat');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Persian');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Labrador');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Multi-animal!');

    // Admins can restrict which vocabularies should be exposed.
    $this->drupalGet('/admin/config/content/views-block-context-provider-settings');
    $assert_session->pageTextContains('Choose the vocabularies to be exposed as contexts. If none are selected, all are allowed.');
    $assert_session->checkboxNotChecked('exposed_vocabularies[goats]');
    $page->checkField('exposed_vocabularies[goats]');
    $page->pressButton('Save configuration');
    $assert_session->pageTextContains('The configuration options have been saved');
    $assert_session->checkboxChecked('exposed_vocabularies[goats]');

    // Cat and Dog no longer appear as select elements.
    $block_selector = '.field--name-layout-builder__layout .block-views-blockvbcp-node-test-block-1';
    $this->visitLayoutAndOpenBlockConfigForm($page_node, $block_selector);
    $assert_session->elementExists('css', '#drupal-off-canvas select[name="settings[override][items_per_page]"]');
    $assert_session->elementNotExists('css', '#drupal-off-canvas select[name="settings[context_mapping][field_cat_type_target_id]"]');
    $assert_session->elementNotExists('css', '#drupal-off-canvas select[name="settings[context_mapping][field_dog_type_target_id]"]');
    $goat_select = $assert_session->elementExists('css', '#drupal-off-canvas select[name="settings[context_mapping][field_goat_type_target_id]"]');
    $this->assertSame(2, count($goat_select->findAll('css', 'option')));
    $selected_goat_option = $assert_session->elementExists('css', 'option[selected="selected"]', $goat_select);
    $this->assertSame('Alpine', $selected_goat_option->getText());
    $alpine_option = $assert_session->elementExists('css', "option[value='@views_block_context_provider.term_context:{$term_alpine->uuid()}']", $goat_select);
    $this->assertSame('Alpine', $alpine_option->getText());
    $page->selectFieldOption('Goat type', "@views_block_context_provider.term_context:{$term_alpine->uuid()}");
    $page->pressButton('Update');
    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas', 5000);
    $assert_session->pageTextContains('You have unsaved changes');
    $this->saveHtmlOutput();

    // Preview now has only Goat and Multi-animal.
    $assert_session->pageTextNotContains('This node should never show up');
    $block_selector = '.field--name-layout-builder__layout .block-views-blockvbcp-node-test-block-1';
    $block_preview = $assert_session->elementExists('css', $block_selector);
    $views_rows = $block_preview->findAll('css', '.views-row');
    $this->assertSame(2, count($views_rows));
    $assert_session->elementTextContains('css', $block_selector, 'vbcp_node_test');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Siamese');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Boxer');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Goat');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Persian');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Labrador');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Multi-animal!');

    // Front-end too.
    $page->pressButton('Save layout');
    $assert_session->pageTextContains('The layout override has been saved');
    $block_selector = '.layout__region--content .block-views-blockvbcp-node-test-block-1';
    $block_rendered = $assert_session->elementExists('css', $block_selector);
    $views_rows = $block_rendered->findAll('css', '.views-row');
    $this->assertSame(2, count($views_rows));
    $assert_session->elementTextContains('css', $block_selector, 'vbcp_node_test');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Siamese');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Boxer');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Goat');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Persian');
    $assert_session->elementTextNotContains('css', $block_selector, 'Im a Labrador');
    $assert_session->elementTextContains('css', $block_selector, 'Im a Multi-animal!');

    // Make sure our contexts don't leak to other places, such as the standard
    // block placement UI.
    $this->drupalGet('/admin/structure/block/add/views_block%3Avbcp_node_test-block_1/');
    $assert_session->elementExists('css', 'select[name="settings[override][items_per_page]"]');
    $assert_session->elementNotExists('css', 'select[name="settings[context_mapping][field_cat_type_target_id]"]');
    $assert_session->elementNotExists('css', 'select[name="settings[context_mapping][field_dog_type_target_id]"]');
    $assert_session->elementNotExists('css', 'select[name="settings[context_mapping][field_goat_type_target_id]"]');
  }

  /**
   * Debugger method to save additional HTML output.
   *
   * The base class will only save browser output when accessing page using
   * ::drupalGet and providing a printer class to PHPUnit. This method
   * is intended for developers to help debug browser test failures and capture
   * more verbose output.
   */
  private function saveHtmlOutput() {
    $out = $this->getSession()->getPage()->getContent();
    // Ensure that any changes to variables in the other thread are picked up.
    $this->refreshVariables();
    if ($this->htmlOutputEnabled) {
      $html_output = '<hr />Ending URL: ' . $this->getSession()->getCurrentUrl();
      $html_output .= '<hr />' . $out;
      $html_output .= $this->getHtmlOutputHeaders();
      $this->htmlOutput($html_output);
    }
  }

  /**
   * Helper to visit a node LB override form and open a given block config form.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node we are changing the LB override content.
   * @param string $block_selector
   *   The CSS selector of the block. Must be already-placed in a Layout Builder
   *   region, and be exactly one level up from the '.contextual' element.
   */
  private function visitLayoutAndOpenBlockConfigForm($node, $block_selector) {
    $this->drupalGet("/node/{$node->id()}/layout");
    $this->clickContextualLink($block_selector . ' > .contextual', 'Configure');
    $this->assertNotEmpty($this->assertSession()->waitForElementVisible('css', '#drupal-off-canvas'));
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->saveHtmlOutput();
  }

}
